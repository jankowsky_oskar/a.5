public class Lotto {

    public static void main(String[] args) {
        int[] numbers = { 3, 7, 12, 18, 37, 42 };

        System.out.print("[");

        for (int i = 0; i < numbers.length; i++) {
            System.out.print("\t" + numbers[i]);
        }

        System.out.print("\t]");

    }

}
