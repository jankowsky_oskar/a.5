import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);

      while (true) {
         System.out.print("x: ");
         double x = scanner.nextDouble();
         System.out.println();
         System.out.print("y: ");
         double y = scanner.nextDouble();
         System.out.println();

         if (x == 0 && y == 0) {
            System.out.println("exit");
            break;
         }

         double m = calculateMittelwert(x, y);

         System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);

      }

      scanner.close();
   }

   static double calculateMittelwert(double x, double y) {
      return (x + y) / 2.0;
   }
}
