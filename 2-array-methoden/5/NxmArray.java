public class NxmArray {

    public static void main(String[] args) {

        int[][] arr = getNxmArray(5, 10);

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static boolean checkNxmArray(int[][] arr) {
        boolean valid = true;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] != arr[j][i]) {
                    valid = false;
                }
            }
        }

        return valid;
    }

    public static int[][] getNxmArray(int nAmount, int mAmount) {
        int[][] array = new int[nAmount][mAmount];
        for (int n = 0; n < nAmount; n++) {
            for (int m = 0; m < mAmount; m++) {
                array[n][m] = n * m;
            }
        }
        return array;
    }

}
