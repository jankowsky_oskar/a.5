public class Temperatur {

    public static void main(String[] args) {

        double[][] temperature = getTemperatureTable(5);
        for (int i = 0; i < temperature.length; i++) {
            for (int j = 0; j < temperature[i].length; j++) {
                System.out.print(temperature[i][j] + " ");
            }
            System.out.println();
        }

    }

    public static double[][] getTemperatureTable(int amount) {

        double[][] temperatures = new double[amount][2];

        for (int i = 0; i < temperatures.length; i++) {
            temperatures[i][0] = i * 10;
            temperatures[i][1] = convertFahrenheitToCelsius(i * 10);
        }
        return temperatures;
    }

    public static double convertFahrenheitToCelsius(double fahrenheit) {
        return (5d / 9d) * (fahrenheit - 32);
    }

}
