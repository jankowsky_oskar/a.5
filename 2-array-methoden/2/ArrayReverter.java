import java.util.Arrays;

public class ArrayReverter {

    public static void main(String[] args) {
        String[] zahlen = { "1", "2", "3" };
        System.out.println(Arrays.toString(reverseArray(zahlen)));
    }

    public static <T> T[] reverseArray(T[] arr) {

        for (int i = 0; i < arr.length / 2; i++) {
            T temp = arr[i];
            arr[i] = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = temp;
        }

        return arr;
    }
}
